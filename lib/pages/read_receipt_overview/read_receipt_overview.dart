import 'dart:async';

import 'package:flutter/material.dart';

import 'package:matrix/matrix.dart';

import 'package:fluffychat/pages/chat/read_receipt/read_receipt_extension.dart';
import 'package:fluffychat/utils/date_time_extension.dart';
import 'package:fluffychat/widgets/matrix.dart';
import 'read_receipt_overview_view.dart';

class ReadReceiptOverviewPage extends StatefulWidget {
  const ReadReceiptOverviewPage({super.key});

  @override
  ReadReceiptOverviewController createState() =>
      ReadReceiptOverviewController();
}

class ExpansionPanelItem {
  Map<String, Event> events = {};
  bool eventsLoaded = false;
  Timeline? timeline;
  Room? room;
  bool isExpanded = false;
  StreamSubscription<List<Event>>? eventStreamSubscription;
  bool hasToGiveReadReceipt = false;
  DateTime sortDate = DateTime(2000);

  ExpansionPanelItem(Room this.room);
}

class ReadReceiptOverviewController extends State<ReadReceiptOverviewPage> {
  Map<String, ExpansionPanelItem> panelItems = {};
  final Map<String, Map<String, Event>> _localStorageEvents = {};
  bool roomsLoaded = false;
  Client? _client;

  void loadRooms() async {
    _client = Matrix.of(context).client;
    await _client!.roomsLoading;
    await _client!.accountDataLoading;
    await _client!.readReceiptRequestsLoading;

    for (final room in _client!.rooms) {
      if (_client!.readReceiptRequests.keys.contains(room.id)) {
        final latestReadReceipt =
            _client!.readReceiptRequests[room.id]!.values.toList().last;
        _addPanelItem(room, latestReadReceipt);
      }
    }

    _client!.newReadReceiptRequestCallback = _updateReadReceiptRequests;

    await _getEventsFromLocalStorage();

    // sort items, so rooms in which user has to give
    // read receipts are at the beginning
    _sortPanelItems();

    setState(() {
      panelItems;
      roomsLoaded = true;
    });
  }

  void _addPanelItem(Room room, MatrixEvent latestReadReceipt) {
    final panelItem = ExpansionPanelItem(room);
    panelItem.sortDate = latestReadReceipt.originServerTs;
    panelItem.hasToGiveReadReceipt =
        _client!.roomsWithOpenReadReceipts.contains(room.id);
    panelItems.addAll({room.id: panelItem});
  }

  void _sortPanelItems() {
    final List<ExpansionPanelItem> sortedItems = panelItems.values.toList()
      ..sort((item1, item2) {
        //  0 ... item 1 = item 2
        // -1 ... item1 < item 2
        //  1 ... item 2 < item 1
        if (item1.hasToGiveReadReceipt == item2.hasToGiveReadReceipt) {
          return getItemDateSortIndex(item1, item2);
          //  return 0;
        }
        // item1 < item 2
        else if (item1.hasToGiveReadReceipt && !item2.hasToGiveReadReceipt) {
          return -1;
        }
        // item 2 < item 1
        else {
          return 1;
        }
      });

    panelItems.clear();
    for (final ExpansionPanelItem item in sortedItems) {
      panelItems.addAll({item.room!.id: item});
    }
  }

  int getItemDateSortIndex(ExpansionPanelItem p1, ExpansionPanelItem p2) {
    if (p1.sortDate == p2.sortDate) {
      return 0;
    } else if (p1.sortDate > p2.sortDate) {
      return -1;
    } else {
      return 1;
    }
  }

  Future<void> _getEventsFromLocalStorage() async {
    final Map<String, Map>? localStorageEventsRaw =
        await _client!.database?.getReadReceiptRequiredEvents();

    if (localStorageEventsRaw != null) {
      for (final String key in localStorageEventsRaw.keys) {
        final roomIdAndEventId = key.split('|');
        if (roomIdAndEventId.length > 1 && localStorageEventsRaw[key] != null) {
          final Room? room =
              panelItems.tryGet<ExpansionPanelItem>(roomIdAndEventId[0])?.room;
          if (room != null) {
            final event = _eventFromJson(localStorageEventsRaw[key]!, room);
            if (_localStorageEvents.containsKey(room.id)) {
              // if event.eventId exits already it is overwritten
              _localStorageEvents[room.id]!.addAll({event.eventId: event});
            } else {
              _localStorageEvents.addAll({
                room.id: {event.eventId: event},
              });
            }
          }
        }
      }
    }
  }

  Event _eventFromJson(eventRaw, room) {
    return Event.fromJson(copyMap(eventRaw), room);
  }

  void _updateOpenReadReceipt(ExpansionPanelItem panelItem) {
    panelItem.hasToGiveReadReceipt =
        _client!.roomsWithOpenReadReceipts.contains(panelItem.room!.id);
  }

  void _updateReadReceiptRequests(Room room, MatrixEvent event) async {
    // if room is not already in panelItems, add it
    if (!panelItems.containsKey(room.id)) {
      _addPanelItem(room, event);
    } else {
      // update sort date with latest read receipt event
      panelItems[room.id]!.sortDate = event.originServerTs;
    }

    if (panelItems[room.id]!.timeline == null) {
      panelItems[room.id]!.timeline =
          await _getTimeline(panelItems[room.id]!.room);
    }

    // then add new message
    for (final panelItem in panelItems.values) {
      if (panelItem.room!.id == room.id) {
        await _addParentToMessages(event, panelItem);

        setState(() {
          panelItem.events;
          panelItem.hasToGiveReadReceipt =
              _client!.roomsWithOpenReadReceipts.contains(room.id);
        });

        break;
      }
    }

    _sortPanelItems();
    setState(() {
      panelItems;
    });
  }

  void _loadMessages(ExpansionPanelItem panelItem) async {
    if (!panelItem.eventsLoaded) {
      final String roomId = panelItem.room!.id;

      panelItem.timeline = await _getTimeline(panelItem.room);
      panelItem.events.clear();

      if (_client!.readReceiptRequests.containsKey(roomId)) {
        for (final MatrixEvent mEvent
            in _client!.readReceiptRequests[panelItem.room!.id]!.values) {
          await _addParentToMessages(mEvent, panelItem);
        }
      }

      setState(() {
        panelItem.eventsLoaded = true;
        panelItem.events;
      });
    }
  }

  Future<void> _addParentToMessages(
    MatrixEvent mEvent,
    ExpansionPanelItem panelItem,
  ) async {
    final String? parentId = mEvent.content
        .tryGetMap<String, dynamic>('m.relates_to')
        ?.tryGet<String>("event_id");

    if (parentId != null) {
      final Room room = panelItem.room!;

      final Event? parentEvent =
          await _loadParentEvent(room, parentId, panelItem.timeline!);

      if (parentEvent != null) {
        // add related events as aggregated events to timeline
        await _addAggregatedEventsToTimeline(
          parentEvent,
          mEvent,
          panelItem.timeline!,
          room,
        );

        // events from sync are sorted chronologically up
        // but we need latest event first -> therefore insert(0, ...
        panelItem.events.addAll({parentEvent.eventId: parentEvent});
      }
    }
  }

  Future<void> _addAggregatedEventsToTimeline(
    Event parentEvent,
    MatrixEvent aggregatedEvent,
    Timeline timeline,
    Room room,
  ) async {
    final relations = await parentEvent.getRelations();

    for (final relation in relations) {
      Event relEvent = Event.fromMatrixEvent(relation, room);

      if (relEvent.relationshipType == RelationshipTypes.edit) {
        relEvent = await _decryptEvent(relEvent, room);
      }
      timeline.addAggregatedEvent(relEvent);
    }
  }

  Future<Timeline> _getTimeline(room) async {
    final timeline = await room!.getTimeline();
    // requests keys for all events in timeline
    timeline!.requestKeys(onlineKeyBackupOnly: false);
    return timeline;
  }

  Future<Event?> _loadParentEvent(
    Room room,
    String parentId,
    Timeline timeline,
  ) async {
    final Event? timelineEvent = await timeline.getEventById(parentId);
    if (timelineEvent != null) {
      return timelineEvent;
    }
    // check if event is already in local storage
    final Event? storageEvent = _localStorageEvents
        .tryGetMap<String, dynamic>(room.id)
        ?.tryGet<Event>(parentId);

    if (storageEvent == null) {
      final MatrixEvent parent =
          await room.client.getOneRoomEvent(room.id, parentId);

      final bool? isRedacted = parent.unsigned?.containsKey("redacted_by");
      if (isRedacted != true) {
        Event parentEvent = Event.fromMatrixEvent(parent, room);
        parentEvent = await _decryptEvent(parentEvent, room);

        // add parent to local storage
        await _client!.database
            ?.addReadReceiptRequiredEvent(parentEvent, room.id);
        return parentEvent;
      } else {
        return null;
      }
    } else {
      return storageEvent;
    }
  }

  Future<Event> _decryptEvent(Event parentEvent, Room room) async {
    if (parentEvent.type == EventTypes.Encrypted &&
        room.client.encryptionEnabled) {
      parentEvent =
          await room.client.encryption!.decryptRoomEvent(room.id, parentEvent);
      if (parentEvent.type == EventTypes.Encrypted ||
          parentEvent.messageType == MessageTypes.BadEncrypted ||
          parentEvent.content['can_request_session'] == true) {
        // Await requestKey() here to ensure decrypted message bodies
        await parentEvent.requestKey();
        parentEvent = await room.client.encryption!
            .decryptRoomEvent(room.id, parentEvent);
      }
    }
    return parentEvent;
  }

  void onReadReceipt(
    Event event,
    ExpansionPanelItem panelItem,
    Event message,
  ) async {
    if (message.isNotOwnEvent) {
      if (!message.isReadReceiptGiving) {
        setState(() {
          message.isReadReceiptGiving = true;
        });

        final String? readReceiptEventId =
            await event.giveReadReceipt(panelItem.timeline!);

        // if readReceiptEventId is !null, then the user has given a new read receipt
        if (readReceiptEventId != null) {
          final MatrixEvent readReceiptEvent = await panelItem.room!.client
              .getOneRoomEvent(panelItem.room!.id, readReceiptEventId);

          _addAggregatedEventsToTimeline(
            event,
            readReceiptEvent,
            panelItem.timeline!,
            panelItem.room!,
          );

          await _client!.updateOpenReadReceipts(panelItem.room!.id);
          _updateOpenReadReceipt(panelItem);
          _sortPanelItems();
        }

        message.isReadReceiptGiving = false;
        setState(() {
          message;
        });
      }
    } else {
      event.showReadReceiptListDialog(context, panelItem.timeline!);
    }
  }

  // returns true if process of giving read receipt for current user is in progress
  bool readReceiptInProgress(Event event) {
    if (panelItems.containsKey(event.roomId)) {
      final panelItem = panelItems[event.roomId]!;

      if (panelItem.events.containsKey(event.eventId)) {
        return panelItem.events[event.eventId]!.isReadReceiptGiving;
      }
    }

    return false;
  }

  void expansionCallback(panelIndex, isExpanded) {
    if (panelItems.length > panelIndex) {
      final panelItem = panelItems.values.elementAt(panelIndex);

      setState(() {
        panelItem.isExpanded = isExpanded;
      });

      if (isExpanded) {
        _loadMessages(panelItem);
      }
    }
  }

  @override
  void initState() {
    super.initState();

    loadRooms();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) => ReadReceiptOverviewView(this);
}
