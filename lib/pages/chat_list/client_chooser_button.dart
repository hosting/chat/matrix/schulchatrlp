import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_web_auth_2/flutter_web_auth_2.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:go_router/go_router.dart';
import 'package:matrix/matrix.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:fluffychat/pages/bootstrap/bootstrap_dialog.dart';
import 'package:fluffychat/widgets/avatar.dart';
import 'package:fluffychat/widgets/matrix.dart';
import '../../config/app_config.dart';
import 'chat_list.dart';

class ClientChooserButton extends StatelessWidget {
  final ChatListController controller;

  const ClientChooserButton(this.controller, {super.key});

  List<PopupMenuEntry<Object>> _bundleMenuItems(BuildContext context) {
    final matrix = Matrix.of(context);

    return <PopupMenuEntry<Object>>[
      PopupMenuItem(
        value: SettingsAction.newGroup,
        child: Row(
          children: [
            const Icon(Icons.group_add_outlined),
            const SizedBox(width: 18),
            Text(L10n.of(context)!.createGroup),
          ],
        ),
      ),
      PopupMenuItem(
        value: SettingsAction.archive,
        child: Row(
          children: [
            const Icon(Icons.archive_outlined),
            const SizedBox(width: 18),
            Text(L10n.of(context)!.archive),
          ],
        ),
      ),
      PopupMenuItem(
        value: SettingsAction.settings,
        child: Row(
          children: [
            const Icon(Icons.settings_outlined),
            const SizedBox(width: 18),
            Text(L10n.of(context)!.settings),
          ],
        ),
      ),
      PopupMenuItem(
        value: SettingsAction.requireReadReceipt,
        child: Row(
          children: [
            const Icon(Icons.mark_chat_read_outlined),
            const SizedBox(width: 18),
            Text(L10n.of(context)!.readReceipts),
          ],
        ),
      ),
      PopupMenuItem(
        value: SettingsAction.showAddressbook,
        child: Row(
          children: [
            const Icon(Icons.contacts),
            const SizedBox(width: 18),
            Text(L10n.of(context)!.addressbook),
          ],
        ),
      ),
      //schulchat-specific: show only displayname, seen in https://gitlab.com/famedly/fluffychat/-/blob/v1.11.0/lib/pages/chat_list/client_chooser_button.dart
      const PopupMenuItem(
        value: null,
        child: Divider(height: 1),
      ),
      PopupMenuItem(
        value: matrix.client,
        child: FutureBuilder<Profile?>(
          // analyzer does not understand this type cast for error
          // handling
          //
          // ignore: unnecessary_cast
          future:
              (matrix.client.fetchOwnProfile(getFromRooms: false, cache: true)
                      as Future<Profile?>)
                  .onError((e, s) => null),
          builder: (context, snapshot) => Row(
            children: [
              Avatar(
                mxContent: snapshot.data?.avatarUrl,
                name: snapshot.data?.displayName ?? "",
                size: 32,
                fontSize: 12,
              ),
              const SizedBox(width: 12),
              Expanded(
                child: Text(
                  snapshot.data?.displayName ?? "",
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              const SizedBox(width: 12),
            ],
          ),
        ),
      ),
      //schulchat-specific: add sign out button in client_chooser_button
      PopupMenuItem(
        value: SettingsAction.logout,
        child: Row(
          children: [
            const Icon(Icons.logout_outlined),
            const SizedBox(width: 18),
            Text(L10n.of(context)!.logout),
          ],
        ),
      ),
      if (!(matrix.client.getBackupQuestion()?.startsWith('q') ?? false))
        PopupMenuItem(
          value: SettingsAction.finishBackup,
          child: Row(
            children: [
              const Icon(Icons.warning_sharp),
              const SizedBox(width: 18),
              Text(L10n.of(context)!.finishChatBackup),
            ],
          ),
        ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    final matrix = Matrix.of(context);

    int clientCount = 0;
    matrix.accountBundles.forEach((key, value) => clientCount += value.length);
    return FutureBuilder<Profile>(
      future: matrix.client.fetchOwnProfile(getFromRooms: false, cache: true),
      builder: (context, snapshot) => Stack(
        alignment: Alignment.center,
        children: [
          PopupMenuButton<Object>(
            onSelected: (o) => _clientSelected(o, context),
            itemBuilder: _bundleMenuItems,
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 6, bottom: 4),
                  child: Material(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(99),
                    child: Avatar(
                      mxContent: snapshot.data?.avatarUrl,
                      name: snapshot.data?.displayName ??
                          matrix.client.userID!.localpart,
                      size: 32,
                      fontSize: 12,
                    ),
                  ),
                ),
                if (controller.hasToGiveReadReceipt)
                  const Positioned(
                    bottom: 0,
                    right: 0,
                    child: Icon(
                      Icons.mark_chat_read,
                      color: AppConfig.primaryColor,
                      size: 16,
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _clientSelected(
    Object object,
    BuildContext context,
  ) async {
    if (object is Client) {
      controller.setActiveClient(object);
    } else if (object is String) {
      controller.setActiveBundle(object);
    } else if (object is SettingsAction) {
      switch (object) {
        case SettingsAction.newGroup:
          context.go('/rooms/newgroup');
          break;
        case SettingsAction.settings:
          context.go('/rooms/settings');
          break;
        case SettingsAction.archive:
          context.go('/rooms/archive');
          break;
        case SettingsAction.requireReadReceipt:
          context.go('/rooms/readreceipts');
          break;
        case SettingsAction.showAddressbook:
          context.go('/rooms/addressbook');
          break;
        case SettingsAction.finishBackup:
          await BootstrapDialog(
            client: Matrix.of(context).client,
          ).show(context);
          break;
        case SettingsAction
              .logout: //schulchat-specific: add sign out button in client_chooser_button
          final c = Matrix.of(context).client;
          final encEnabled =
              c.encryption?.keyManager.enabled == true && !(c.isUnknownSession);
          if (await showOkCancelAlertDialog(
                useRootNavigator: false,
                context: context,
                title: L10n.of(context)!.areYouSureYouWantToLogout,
                message: encEnabled ? '' : L10n.of(context)!.noBackupWarning,
                isDestructiveAction: true,
                okLabel: L10n.of(context)!.logout,
                cancelLabel: L10n.of(context)!.cancel,
              ) ==
              OkCancelResult.cancel) {
            return;
          }
          await showFutureLoadingDialog(
            context: context,
            // future: () => matrix.client.logout(),
            future: () async {
              final matrix = Matrix.of(context);
              try {
                if (kIsWeb) {
                  launchUrl(Uri.parse(AppConfig.idpLogoutUrl));
                } else {
                  // Workaround using Webview
                  await FlutterWebAuth2.authenticate(
                    url: AppConfig.idpLogoutUrl,
                    callbackUrlScheme: 'https',
                  );
                }
                // retry logout?
              } catch (_) {}

              await matrix.client.logout();
            },
          );
          break;
      }
    }
  }
}

enum SettingsAction {
  newGroup,
  settings,
  archive,
  requireReadReceipt,
  showAddressbook,
  finishBackup,
  logout
}
