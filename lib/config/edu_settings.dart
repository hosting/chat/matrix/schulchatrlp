abstract class EduSettings {
  static const String eduNamespace = 'edu.matrix.klassenfunk';
  static const String requireReadReceipt = 'require-read-receipt';
  static const String readReceiptGiven = 'read-receipt';
  static const bool disableAuthWithUsernameAndPassword = true;
}
