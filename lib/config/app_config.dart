import 'package:flutter/material.dart';

import 'package:matrix/matrix.dart';

abstract class AppConfig {
  static String _applicationName = 'Schulchat RLP';
  static String get applicationName => _applicationName;
  static String? _applicationWelcomeMessage;
  static String? get applicationWelcomeMessage => _applicationWelcomeMessage;
  static String _defaultHomeserver = 'schulchat.rlp.de';
  static String get defaultHomeserver => _defaultHomeserver;
  static double fontSizeFactor = 1;
  static const double messageFontSize = 15.75;
  static const bool allowOtherHomeservers = false;
  static const bool enableRegistration = false;
  static const Color primaryColor = Color(0xFF009CEE);
  static const Color primaryColorLight = Color(0xFFFFFFFF);
  static const Color secondaryColor = Color(0xFF009cee);
  static const Color chatColor = primaryColor;
  static Color? colorSchemeSeed = primaryColor;
  static String _privacyUrl =
      'https://infoportal.schulcampus-rlp.de/datenschutzerklaerung/';
  static String get privacyUrl => _privacyUrl;
  static const String enablePushTutorial =
      'https://github.com/krille-chan/fluffychat/wiki/Push-Notifications-without-Google-Services';
  static const String encryptionTutorial =
      'https://github.com/krille-chan/fluffychat/wiki/How-to-use-end-to-end-encryption-in-FluffyChat';
  static const String startChatTutorial =
      'https://github.com/krille-chan/fluffychat/wiki/How-to-Find-Users-in-FluffyChat';
  static const String appId = 'im.fluffychat.FluffyChat';
  static const String appOpenUrlScheme = 'im.fluffychat';
  static String _webBaseUrl = 'https://schulchat.rlp.de/web';
  static String get webBaseUrl => _webBaseUrl;
  static const String sourceCodeUrl =
      'https://git.fairkom.net/chat/matrix/schulchatrlp/';
  static const String supportUrl =
      'https://infoportal.schulcampus-rlp.de/lehrende/schulchat/schulchat-rlp-einfuehrung/';
  static final Uri newIssueUrl = Uri(
    scheme: 'https',
    host: 'gitlab.com',
    path: '/famedly/fluffychat/-/issues/new',
  );
  static bool renderHtml = true;
  static bool hideRedactedEvents = false;
  static bool hideUnknownEvents = true;
  static bool hideUnimportantStateEvents = true;
  static bool separateChatTypes = false;
  static bool sendTypingNotifications = true;
  static bool autoplayImages = true;
  static bool sendOnEnter = false;
  static const bool hideTypingUsernames = false;
  static const bool hideAllStateEvents = false;
  static const String inviteLinkPrefix = 'https://matrix.to/#/';
  static const String deepLinkPrefix = 'im.fluffychat://chat/';
  static const String schemePrefix = 'matrix:';
  static const String pushNotificationsChannelId = 'schulchat_push';
  static const String pushNotificationsChannelName = 'Schulchat push channel';
  static const String pushNotificationsChannelDescription =
      'Push notifications for Schulchat';
  static const String pushNotificationsAppId = 'de.rlp.schulchat';
  static const String pushNotificationsGatewayUrl =
      'https://schulchat.dev.osalliance.com/_matrix/push/v1/notify';
  static const String pushNotificationsPusherFormat = 'event_id_only';
  static const String emojiFontName = 'Noto Emoji';
  static const String emojiFontUrl =
      'https://github.com/googlefonts/noto-emoji/';
  static const double borderRadius = 16.0;
  static const double columnWidth = 360.0;
  static String idpLogoutUrl = 'https://bildungsportal.rlp.de/oauth2/logout';

  static void loadFromJson(Map<String, dynamic> json) {
    if (json['chat_color'] != null) {
      try {
        colorSchemeSeed = Color(json['chat_color']);
      } catch (e) {
        Logs().w(
          'Invalid color in config.json! Please make sure to define the color in this format: "0xffdd0000"',
          e,
        );
      }
    }
    if (json['application_name'] is String) {
      _applicationName = json['application_name'];
    }
    if (json['application_welcome_message'] is String) {
      _applicationWelcomeMessage = json['application_welcome_message'];
    }
    if (json['default_homeserver'] is String) {
      _defaultHomeserver = json['default_homeserver'];
    }
    if (json['privacy_url'] is String) {
      _privacyUrl = json['privacy_url'];
    }
    if (json['web_base_url'] is String) {
      _webBaseUrl = json['web_base_url'];
    }
    if (json['render_html'] is bool) {
      renderHtml = json['render_html'];
    }
    if (json['hide_redacted_events'] is bool) {
      hideRedactedEvents = json['hide_redacted_events'];
    }
    if (json['hide_unknown_events'] is bool) {
      hideUnknownEvents = json['hide_unknown_events'];
    }
    if (json['idpLogoutUrl'] is String) {
      idpLogoutUrl = json['idpLogoutUrl'];
    }
  }

  static const ColorScheme colorSchemeLight = ColorScheme(
    brightness: Brightness.light,
    primary: Color(0xFF009CEE),
    onPrimary: Color(0xFFFFFFFF),
    primaryContainer: Color(0xFF009CEE),
    onPrimaryContainer: Color(0xFF21005D),
    onSecondary: Color(0xFFFFFFFF),
    secondary: Color(0xFF625B71),
    secondaryContainer: Color(0xFFA9CAE5),
    onSecondaryContainer: Color(0xFF1D192B),
    tertiary: Color(0xFF7D5260),
    onTertiary: Color(0xFFFFFFFF),
    tertiaryContainer: Color(0xFFFFD8E4),
    onTertiaryContainer: Color(0xFF31111D),
    error: Color(0xFFB3261E),
    onError: Color(0xFFFFFFFF),
    errorContainer: Color(0xFFF9DEDC),
    onErrorContainer: Color(0xFF410E0B),
    background: Color(0xFFFFFBFE),
    onBackground: Color(0xFF1C1B1F),
    surface: Color(0xFFFFFBFE),
    onSurface: Color(0xFF1C1B1F),
    surfaceVariant: Color(0xFFE7E0EC),
    onSurfaceVariant: Color(0xFF49454F),
    outline: Color(0xFF79747E),
    outlineVariant: Color(0xFFCAC4D0),
    shadow: Color(0xFF000000),
    scrim: Color(0xFF000000),
    inverseSurface: Color(0xFF313033),
    onInverseSurface: Color(0xFFF4EFF4),
    inversePrimary: Color(0xFFD0BCFF),
    // The surfaceTint color is set to the same color as the primary.
    surfaceTint: Color(0xFF009CEE),
  );

  static const ColorScheme colorSchemeDark = ColorScheme(
    brightness: Brightness.dark,
    primary: Color(0xFF009CEE),
    onPrimary: Color(0xFFFFFFFF),
    primaryContainer: Color(0xFF009CEE),
    onPrimaryContainer: Color(0xFFEADDFF),
    secondary: Color(0xFFCCC2DC),
    onSecondary: Color(0xFF332D41),
    secondaryContainer: Color(0xFF4A4458),
    onSecondaryContainer: Color(0xFFE8DEF8),
    tertiary: Color(0xFFEFB8C8),
    onTertiary: Color(0xFF492532),
    tertiaryContainer: Color(0xFF633B48),
    onTertiaryContainer: Color(0xFFFFD8E4),
    error: Color(0xFFF2B8B5),
    onError: Color(0xFF601410),
    errorContainer: Color(0xFF8C1D18),
    onErrorContainer: Color(0xFFF9DEDC),
    background: Color(0xFF1C1B1F),
    onBackground: Color(0xFFE6E1E5),
    surface: Color(0xFF1C1B1F),
    onSurface: Color(0xFFE6E1E5),
    surfaceVariant: Color(0xFF49454F),
    onSurfaceVariant: Color(0xFFCAC4D0),
    outline: Color(0xFF938F99),
    outlineVariant: Color(0xFF49454F),
    shadow: Color(0xFF000000),
    scrim: Color(0xFF000000),
    inverseSurface: Color(0xFFE6E1E5),
    onInverseSurface: Color(0xFF313033),
    inversePrimary: Color(0xFF6750A4),
    // The surfaceTint color is set to the same color as the primary.
    surfaceTint: Color(0xFF009CEE),
  );
}
