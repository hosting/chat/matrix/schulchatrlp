import 'package:matrix/matrix.dart';

import '../../config/app_config.dart';

extension IsStateExtension on Event {
  bool isProvisioningBotEvent() {
    final bool isRoomMember = type == EventTypes.RoomMember;
    final bool isFromProvisioningBot =
        stateKey?.localpart == 'idm_provisioning_bot' ||
            senderId.startsWith('@idm_provisioning_bot');

    return isRoomMember && isFromProvisioningBot;
  }

  bool isPowerLevelModerator() {
    return room.ownPowerLevel >= 100;
  }

  bool get isVisibleInGui =>
      // always filter out edit and reaction relationships
      !{RelationshipTypes.edit, RelationshipTypes.reaction}
          .contains(relationshipType) &&
      // always filter out m.key.* events
      !type.startsWith('m.key.verification.') &&
      // event types to hide: redaction and reaction events
      // if a reaction has been redacted we also want it to be hidden in the timeline
      !{EventTypes.Reaction, EventTypes.Redaction}.contains(type) &&
      // if we enabled to hide all redacted events, don't show those
      (!AppConfig.hideRedactedEvents || !redacted) &&
      // always filter out read receipt redactions of edit events
      (!redacted ||
          !(unsigned
                  ?.tryGetMap<String, dynamic>("redacted_because")
                  ?.tryGetMap<String, dynamic>("content")
                  ?.tryGet<String>("reason") ==
              'Read receipt response discarded due to message edit.')) &&
      // if we enabled to hide all unknown events, don't show those
      (!AppConfig.hideUnknownEvents || isEventTypeKnown) &&
      // remove state events that we don't want to render
      (isState || !AppConfig.hideAllStateEvents) &&
      // hide unimportant state events
      (!AppConfig.hideUnimportantStateEvents ||
          !isState ||
          importantStateEvents.contains(type)) &&
      // hide simple join/leave member events in public rooms
      (!AppConfig.hideUnimportantStateEvents ||
          type != EventTypes.RoomMember ||
          room.joinRules != JoinRules.public ||
          content.tryGet<String>('membership') == 'ban' ||
          stateKey != senderId) &&
      !(!isPowerLevelModerator() && isProvisioningBotEvent());

  static const Set<String> importantStateEvents = {
    EventTypes.Encryption,
    EventTypes.RoomCreate,
    EventTypes.RoomMember,
    EventTypes.RoomTombstone,
  };

  bool get isState => !{
        EventTypes.Message,
        EventTypes.PollStart,
        EventTypes.Sticker,
        EventTypes.Encrypted,
      }.contains(type);
}
