import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:go_router/go_router.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:url_launcher/url_launcher_string.dart';

import '../config/app_config.dart';

abstract class PlatformInfos {
  static bool get isWeb => kIsWeb;

  static bool get isLinux => !kIsWeb && Platform.isLinux;

  static bool get isWindows => !kIsWeb && Platform.isWindows;

  static bool get isMacOS => !kIsWeb && Platform.isMacOS;

  static bool get isIOS => !kIsWeb && Platform.isIOS;

  static bool get isAndroid => !kIsWeb && Platform.isAndroid;

  static bool get isCupertinoStyle => isIOS || isMacOS;

  static bool get isMobile => isAndroid || isIOS;

  /// For desktops which don't support ChachedNetworkImage yet
  static bool get isBetaDesktop => isWindows || isLinux;

  static bool get isDesktop => isLinux || isWindows || isMacOS;

  static bool get usesTouchscreen => !isMobile;

  static bool get platformCanRecord => (isMobile || isMacOS);

  static String get clientName =>
      '${AppConfig.applicationName} ${isWeb ? 'web' : Platform.operatingSystem}${kReleaseMode ? '' : 'Debug'}';

  static Future<String> getVersion() async {
    var version = kIsWeb ? 'Web' : 'Unknown';
    try {
      version = (await PackageInfo.fromPlatform()).version;
    } catch (_) {}
    return version;
  }

  static void showDialog(BuildContext context) async {
    final version = await PlatformInfos.getVersion();
    showAboutDialog(
      context: context,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Version: $version'),
            SizedBox(height: 8),
            Text(L10n.of(context)!.schulchatInfo),
            Text(L10n.of(context)!.fairkomInfo),
            SizedBox(height: 8),
            Container(
              width: double.infinity,
              child: TextButton.icon(
                onPressed: () =>
                    launchUrlString("https://infoportal.schulcampus-rlp.de"),
                icon: const Icon(Icons.info_outline),
                label: Text(L10n.of(context)!.infoSchulchat),
                style: TextButton.styleFrom(alignment: Alignment.centerLeft),
              ),
            ),
            Container(
              width: double.infinity,
              child: TextButton.icon(
                onPressed: () => launchUrlString(
                    "https://infoportal.schulcampus-rlp.de/service/faq-schulchat-rlp/"),
                icon: const Icon(Icons.question_mark),
                label: Text(L10n.of(context)!.faqSchulchat),
                style: TextButton.styleFrom(alignment: Alignment.centerLeft),
              ),
            ),
            Container(
              width: double.infinity,
              child: TextButton.icon(
                onPressed: () =>
                    launchUrlString("mailto:schulcampus@pl.rlp.de"),
                icon: const Icon(Icons.email),
                label: Text(L10n.of(context)!.helpSchulchat),
                style: TextButton.styleFrom(alignment: Alignment.centerLeft),
              ),
            ),
            Container(
              width: double.infinity,
              child: TextButton.icon(
                onPressed: () => launchUrlString(AppConfig.sourceCodeUrl),
                icon: const Icon(Icons.source_outlined),
                label: Text(L10n.of(context)!.sourceCode),
                style: TextButton.styleFrom(alignment: Alignment.centerLeft),
              ),
            ),
            Container(
              width: double.infinity,
              child: TextButton.icon(
                onPressed: () => launchUrlString(AppConfig.emojiFontUrl),
                icon: const Icon(Icons.emoji_emotions_outlined),
                label: const Text(AppConfig.emojiFontName),
                style: TextButton.styleFrom(alignment: Alignment.centerLeft),
              ),
            ),
            Builder(
              builder: (innerContext) {
                return Container(
                  width: double.infinity,
                  child: TextButton.icon(
                    onPressed: () {
                      context.go('/logs');
                      Navigator.of(innerContext).pop();
                    },
                    icon: const Icon(Icons.list_outlined),
                    label: const Text('Logs'),
                    style:
                        TextButton.styleFrom(alignment: Alignment.centerLeft),
                  ),
                );
              },
            ),
          ],
        ),
      ],
      applicationIcon: Image.asset(
        'assets/logo.png',
        width: 64,
        height: 64,
        filterQuality: FilterQuality.medium,
      ),
      applicationName: AppConfig.applicationName,
    );
  }
}
