#!/bin/sh -ve
rm -r assets/js/package

# OLM_VERSION=$(cat pubspec.yaml | yq .dependencies.flutter_olm)
OLM_VERSION=1.3.2  # TODO: This is currently a hardcoded OLM version. Ideally, the version should be retrieved dynamically from the pubspec.yaml file, but this method is not working as expected. Please update this later.
DOWNLOAD_PATH="https://github.com/famedly/olm/releases/download/v$OLM_VERSION/olm.zip"

cd assets/js/ && curl -L $DOWNLOAD_PATH > olm.zip && cd ../../
cd assets/js/ && unzip olm.zip && cd ../../
cd assets/js/ && rm olm.zip && cd ../../
cd assets/js/ && mv javascript package && cd ../../
