![](https://git.fairkom.net/chat/matrix/schulchatrlp/-/raw/main/assets/info-logo.png)

SchulchatRLP is the chat messenger for schools in Rheinland-Pfalz (Germany) using the Matrix messenger protocol. 

The client for Android, iOS and Web is based on a fork of FluffyChat.  

<a href="https://matrix.to/#/#fluffychat:matrix.org" target="new">Join the FluffyChat community</a> - <a href="https://metalhead.club/@krille" target="new">Follow FluffyChat on Mastodon</a> - <a href="https://hosted.weblate.org/projects/fluffychat/" target="new">Translate FluffyChat</a> - <a href="https://fluffychat.im" target="new">FluffyChat Website</a> - <a href="https://gitlab.com/famedly/famedlysdk" target="new">Famedly Matrix SDK</a> 

SchulchatRLP has been developed by <a href="https://fairkom.eu">fairkom</a>. 

FluffyChat is an open source, nonprofit matrix messenger app. The app is easy to use but secure and decentralized.

## FluffyChat core features

- Send all kinds of messages, images and files
- Voice messages
- Push notifications
- Public channels with thousands of participants
- Feature rich group moderation including all matrix features
- Dark mode
- Custom themes
- Hides complexity of Matrix IDs behind simple QR codes
- Custom emotes and stickers
- Compatible with Element, Nheko, NeoChat and all other Matrix apps
- End to end encryption
- Emoji verification & cross signing

### Added features

For education environments, we have added in SchulchatRLP:

- Reading confirmation
- Polls

### Removed features 

- Location sharing
- Spaces
- Unlimited private and public group chats
- Discover and join public groups

# Installation

The SchulchatRLP app is available in the Google / Apple Stores. Usage requires an account on https://bildungsportal.rlp.de

Please visit the Fluffychat website for further installation instructions: https://fluffychat.im (but use this repo).

## How to build

Please visit the Fluffychat Wiki for build instructions: https://gitlab.com/famedly/fluffychat/-/wikis/How-To-Build (but use this repo). Desktop builds have not been tested. 


# Special thanks

* <a href="https://fluffychat.im" target="new">FluffyChat developers</a> 

* <a href="https://www.famedly.com/" target="new">Famedly</a> 

* <a href="https://github.com/fabiyamada">Fabiyamada</a> is a graphics designer from Brasil and has made the fluffychat logo and the banner. Big thanks for her great designs.

* <a href="https://github.com/advocatux">Advocatux</a> has made the Spanish translation with great love and care. He always stands by my side and supports my work with great commitment.

* Thanks to MTRNord and Sorunome for developing.

* Also thanks to all translators and testers! With your help, fluffychat is now available in more than 12 languages.

* <a href="https://github.com/googlefonts/noto-emoji/">Noto Emoji Font</a> for the awesome emojis.

* <a href="https://github.com/madsrh/WoodenBeaver">WoodenBeaver</a> sound theme for the notification sound.

* The <a href="https://www.matrix.org/foundation/">Matrix Foundation</a> for leading the protocol specification

* The Matrix Foundation for making and maintaining the [emoji translations](https://github.com/matrix-org/matrix-doc/blob/main/data-definitions/sas-emoji.json) used for emoji verification, licensed Apache 2.0