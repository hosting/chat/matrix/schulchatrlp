name: fluffychat
description: Schulchat für Rheinland-Pfalz
publish_to: none
version: 33.0.13+78

environment:
  sdk: ">=3.0.0 <4.0.0"

dependencies:
  adaptive_dialog: ^2.0.0 #adaptive_dialog: ^2.0.0 update
  #  git:
  #    url: git@git.fairkom.net:clients/rlp/client/adaptive_dialog
  #    ref: main
  animations: ^2.0.8
  archive: ^3.4.9
  async: ^2.11.0
  badges: ^3.1.2
  blurhash_dart: ^1.2.1
  callkeep: ^0.3.2
  chewie: ^1.7.1
  collection: ^1.17.2
  cupertino_icons: any
  desktop_drop: ^0.4.4
  desktop_notifications: ^0.6.3
  device_info_plus: ^9.1.0
  dynamic_color: ^1.6.8
  emoji_picker_flutter: ^1.6.3
  emoji_proposal: ^0.0.1
  emojis: ^0.9.9
  fcm_shared_isolate: ^0.1.0
  file_picker: ^6.1.1
  flutter:
    sdk: flutter
  flutter_app_badger: ^1.5.0 # discontinued.
  flutter_blurhash: ^0.8.2
  flutter_cache_manager: ^3.3.0
  flutter_fancy_tree_view: ^1.1.0
  flutter_foreground_task: ^6.0.0+1
  flutter_highlighter: ^0.1.1
  flutter_html: ^3.0.0-beta.2
  flutter_html_table: ^3.0.0-beta.2
  flutter_linkify: ^6.0.0
  flutter_local_notifications: ^16.1.0
  flutter_localizations:
    sdk: flutter
  flutter_map: ^4.0.0
  flutter_math_fork: ^0.7.2
  flutter_olm: 1.3.2 # Keep in sync with scripts/prepare-web.sh ! 1.4.0 does currently not build on Android
  flutter_openssl_crypto:
    git:
      url: git@git.fairkom.net/hosting/chat/matrix/flutter-openssl-crypto
      ref: main
  flutter_polls:
    git:
      url: git@git.fairkom.net/hosting/chat/matrix/adaptive_dialog.git
      ref: main
  flutter_ringtone_player: ^4.0.0+2
  flutter_secure_storage: ^9.0.0
  flutter_typeahead: ^4.8.0
  flutter_web_auth: ^0.5.0
  flutter_web_auth_2: ^3.0.3
  flutter_webrtc: ^0.9.46
  future_loading_dialog: ^0.3.0
  go_router: ^12.1.1
  handy_window: ^0.3.1
  hive: ^2.2.3
  hive_flutter: ^1.1.0
  http: ^0.13.6
  image_picker: ^1.0.0
  intl: any
  just_audio: ^0.9.30
  keyboard_shortcuts: ^0.1.4
  latlong2: ^0.8.1
  linkify: ^5.0.0
  matrix:
    git:
      url: git@git.fairkom.net/hosting/chat/matrix/famedlysdk
      ref: main
  matrix_homeserver_recommendations:
    git:
      url: git@git.fairkom.net:clients/rlp/client/matrix_homeserver_recommendations.git
      ref: main
  matrix_link_text: ^1.0.2
  native_imaging: ^0.1.1
  package_info_plus: ^4.0.0
  pasteboard: ^0.2.0
  path_provider: ^2.0.9
  permission_handler: ^11.0.1
  provider: ^6.0.2
  punycode: ^1.0.0
  qr_code_scanner: ^1.0.0
  qr_flutter: ^4.0.0
  receive_sharing_intent: ^1.4.5
  record: 4.4.4 # Upgrade to 5 currently breaks playing on iOS
  scroll_to_index: ^3.0.1
  share_plus: ^7.2.1
  shared_preferences: ^2.2.0 # Pinned because https://github.com/flutter/flutter/issues/118401
  slugify: ^2.0.0
  swipe_to_action: ^0.2.0
  tor_detector_web: ^1.1.0
  uni_links: ^0.5.1 #discontinued.
  unifiedpush: ^5.0.1
  universal_html: ^2.2.4
  url_launcher: ^6.2.1
  vibration: ^1.8.3
  video_compress: 3.1.1
  video_player: ^2.8.1
  wakelock_plus: ^1.1.3

dev_dependencies:
  # dart_code_metrics: ^5.7.5 discontinued.
  flutter_lints: ^3.0.0
  flutter_native_splash: ^2.0.3+1
  flutter_test:
    sdk: flutter
  import_sorter: ^4.6.0
  integration_test:
    sdk: flutter
  msix: ^3.6.2
  translations_cleaner: ^0.0.5

flutter_native_splash:
  color: "#ffffff"
  color_dark: "#000000"
  image: "assets/info-logo.png"

flutter:
  generate: true
  uses-material-design: true
  assets:
    - assets/
    - assets/sounds/
    - assets/js/
    - assets/js/package/

  fonts:
    - family: Roboto
      fonts:
        - asset: fonts/Roboto/Roboto-Regular.ttf
        - asset: fonts/Roboto/Roboto-Italic.ttf
          style: italic
        - asset: fonts/Roboto/Roboto-Bold.ttf
          weight: 700
    - family: RobotoMono
      fonts:
        - asset: fonts/Roboto/RobotoMono-Regular.ttf
    - family: NotoEmoji
      fonts:
        - asset: fonts/NotoEmoji/NotoColorEmoji.ttf
        - asset: fonts/NotoEmoji/bMrnmSyK7YY-MEu6aWjPDs-ar6uWaGWuob-r0jwvS-FGJCMY.ttf

msix_config:
  display_name: FluffyChat
  publisher_display_name: FluffyChat
  publisher: CN=FluffyChat, O=Head of bad integration tests, L=Matrix, S=Internet, C=EU
  identity_name: de.rlp.schulchat
  logo_path: assets\logo.png
  capabilities: internetClient, location, microphone, webcam
  protocol_activation: https
  app_uri_handler_hosts: fluffychat.im, matrix.to
  execution_alias: fluffychat
  sign_msix: false
  install_certificate: false

dependency_overrides:
  flutter_secure_storage_linux: 1.1.3
  # fake secure storage plugin for Windows
  # See: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/15161
  flutter_secure_storage_windows:
    git:
      url: https://gitlab.com/TheOneWithTheBraid/flutter_secure_storage_windows.git
      ref: main
  # waiting for null safety
  # Upstream pull request: https://github.com/AntoineMarcel/keyboard_shortcuts/pull/13
  keyboard_shortcuts:
    git:
      url: git@git.fairkom.net:clients/rlp/client/keyboard_shortcuts.git
      ref: fix-typing-in-chat
  matrix_api_lite:
    git:
      url: git@git.fairkom.net/hosting/chat/matrix/matrix_api_lite.git
      ref: main
  wakelock_windows: ^0.2.1 #Wakelock is Flutter plugin that allows you to keep the device screen awake, i.e. prevent the screen from sleeping.
  win32: ^3.0.0



