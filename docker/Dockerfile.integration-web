FROM debian:bullseye-slim

ARG CI_COMMIT_SHA
ARG CITOKEN
ARG INTEGRATION_USER1
ARG INTEGRATION_PASSWORD1
ARG INTEGRATION_USER2
ARG INTEGRATION_PASSWORD2
ARG HOMESERVER

ENV FLUTTER_VERSION=3.16.2
ENV INTEGRATION_USER1=$INTEGRATION_USER1
ENV INTEGRATION_PASSWORD1=$INTEGRATION_PASSWORD1
ENV INTEGRATION_USER2=$INTEGRATION_USER2
ENV INTEGRATION_PASSWORD2=$INTEGRATION_PASSWORD2
ENV HOMESERVER=$HOMESERVER

RUN echo 'deb http://deb.debian.org/debian bullseye-backports main' >> /etc/apt/sources.list
RUN apt update
RUN apt install --no-install-recommends -y ninja-build curl xz-utils git unzip gnupg ca-certificates jq
RUN curl -L -sS https://dl.google.com/linux/linux_signing_key.pub| gpg --dearmor | tee /etc/apt/trusted.gpg.d/google.gpg
RUN echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' >>/etc/apt/sources.list
RUN apt update
RUN apt install google-chrome-stable -y --no-install-recommends
RUN useradd -m -s /bin/bash fluffy
USER fluffy
WORKDIR /home/fluffy
RUN git clone https://gitlab-ci-token:$CITOKEN@git.fairkom.net/clients/rlp/client/flutter.git /tmp/flutter
RUN cd /tmp/flutter && git checkout -b upstream-${FLUTTER_VERSION} origin/upstream-${FLUTTER_VERSION}

# triggers rebuilt
RUN rm -rf /tmp/flutter/bin
RUN cd /tmp/flutter && git checkout bin
ENV PATH=${PATH}:/tmp/flutter/bin
RUN git clone https://gitlab-ci-token:$CITOKEN@git.fairkom.net/clients/rlp/client/fluffychat.git
WORKDIR ./fluffychat
RUN git checkout $CI_COMMIT_SHA

# we use tokens instead of ssh
RUN sed -i "s#git@git.fairkom.net:#https://gitlab-ci-token:$CITOKEN@git.fairkom.net/#g" pubspec.yaml

# local domain is synapse
RUN sed -i "s#schulchat.rlp.de#synapse#g" config.sample.json lib/config/app_config.dart

# no https support for our local synapse
RUN sed -i 's#homeserver = Uri.https#homeserver = Uri.http#g' lib/pages/homeserver_picker/homeserver_picker.dart
RUN sed -i 's#var newDomain = Uri.https#var newDomain = Uri.http#g' lib/pages/login/login.dart

# currently no idm login is done
RUN sed -i 's#disableAuthWithUsernameAndPassword = true#disableAuthWithUsernameAndPassword = false#g' lib/config/edu_settings.dart

RUN mkdir -p assets/js/package
RUN ./scripts/prepare-web.sh

RUN curl -s 'https://storage.googleapis.com/chrome-for-testing-public/126.0.6478.126/linux64/chromedriver-linux64.zip' >/tmp/chromedriver.zip && unzip /tmp/chromedriver.zip && rm /tmp/chromedriver.zip
RUN chmod +x chromedriver-linux64/chromedriver
RUN flutter config --enable-web
COPY ./docker/run-integration-web.sh /
ENTRYPOINT ["/run-integration-web.sh"]
