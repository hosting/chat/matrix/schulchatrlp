**In welcher Version tritt der Fehler auf?** Die Version finden Sie unter Settings -> About

**In welchem Client tritt der Fehler auf?**

- [ ] Web client unter https://web.rlp.fairmatrix.net
- [ ] Android
- [ ] iOS

**Falls Sie den Web client nutzen, welchen Browser verwenden Sie?**
- Browser (inkl. Version):
- Betriebssystem:

**Falls Sie eine native App nutzen, mit welchem Smartphone?**
- Gerätename:

**Beschreiben Sie bitten den Fehler möglichst reproduzierbar**

_Ich bin in der App angemeldet und habe den Raum Testchat 5 geöffnet. Nach einem Klick auf <code>...</code> wähle ich "Contacts" aus. Nach einem Drücken auf den Close-Button (Kreuz oben links) gelange ich zur Raumübersicht und nicht zum Raum Testchat 5. Es erscheint der Hinweis "Raum konnte nicht gefunden werden"._

**Zusätzliche Logs**
Sofern Sie den Web client nutzen, bitte wenn möglich die Meldungen aus der Error Console anhängen. Ctrl+Shift+K oder unter MacOS Cmd+Opt+K drücken und alle Zeilen kopieren.

**Zu welcher Uhrzeit trat der Fehler auf?**
Dies ist nicht für alle Fehler relevant, hilft uns allerdings in den Logs eventuelle Meldungen schneller zu finden.

/label ~activity:assessment
